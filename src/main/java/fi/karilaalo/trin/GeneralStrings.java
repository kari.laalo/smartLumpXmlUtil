package fi.karilaalo.trin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GeneralStrings {
	private static final String BUNDLE_NAME = "generalStrs"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	private GeneralStrings() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	public static List<String> getFeds() {
		ArrayList<String> list = new ArrayList<String>();
		for (String key: Collections.list(RESOURCE_BUNDLE.getKeys())) {
			Pattern pat = Pattern.compile("XmlUtil\\.(\\w*)MetadataURL");
			Matcher mat = pat.matcher(key);
			if (mat.matches() && mat.groupCount()>0) {
				list.add(mat.group(1));
			}
		}
		return list;
	}
	
}
