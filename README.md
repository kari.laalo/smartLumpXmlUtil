[![build status](https://gitlab.com/kari.laalo/smartLumpXmlUtil/badges/master/build.svg)](https://gitlab.com/kari.laalo/smartLumpXmlUtil/commits/master)
[![coverage report](https://gitlab.com/kari.laalo/smartLumpXmlUtil/badges/master/coverage.svg)](https://gitlab.com/kari.laalo/smartLumpXmlUtil/commits/master)

# smartLumpXmlUtil

Simple utility class to 

* fetch a SAML XML metadata file
* fetch a list of Service Providers (SPs)
* fetch a list of assertion consumer service (acs) URLs of those SPs
* and resolve the country code of the hosts

This product includes GeoLite2 data created by MaxMind, available from
<a href="http://www.maxmind.com">http://www.maxmind.com</a>.
