package fi.karilaalo.trin;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.record.Country;

public class XmlUtil {
	
	public final static String XPATH_FOR_SPENTITIES =
			"/d:EntitiesDescriptor/d:EntityDescriptor/d:SPSSODescriptor/../@entityID"; //$NON-NLS-1$
	public final static String XPATH_FOR_SP_ACS =
			"/d:EntitiesDescriptor/d:EntityDescriptor[@entityID='entityid']/d:SPSSODescriptor/d:AssertionConsumerService/@Location"; //$NON-NLS-1$

	// http://stackoverflow.com/questions/19589231/can-i-iterate-through-a-nodelist-using-for-each-in-java
	public static List<Node> asList (NodeList n) {
		return n.getLength()==0 ? Collections.<Node>emptyList() : new NodeListWrapper(n);
	}
	
    static final class NodeListWrapper extends AbstractList<Node> implements RandomAccess {

    	private final NodeList list;
    	public NodeListWrapper(NodeList n) {
			this.list = n;
		}
    	
		@Override
		public Node get(int index) {
			return list.item(index);
		}

		@Override
		public int size() {
			return list.getLength();
		}
    }
    
    public static NamespaceContext getMetadataNSCtx() {
    	return new NamespaceContext() {
			@Override
			public Iterator<?> getPrefixes(String namespaceURI) {
				return null;
			}
			
			@Override
			public String getPrefix(String namespaceURI) {
				return null;
			}
			
			@Override
			public String getNamespaceURI(String prefix) {
				switch (prefix) {
					case "d": //$NON-NLS-1$
					case "": //$NON-NLS-1$
						return GeneralStrings.getString("XmlUtil.SamlNSMetadata"); //$NON-NLS-1$
					case "mdui": //$NON-NLS-1$
						return GeneralStrings.getString("XmlUtil.SamlNSMetadataUi"); //$NON-NLS-1$
					case "saml": //$NON-NLS-1$
						return GeneralStrings.getString("XmlUtil.SamlNSAssertion"); //$NON-NLS-1$
					case "xsi": //$NON-NLS-1$
						return GeneralStrings.getString("XmlUtil.XmlNsSchema"); //$NON-NLS-1$
				}
				return null;
			}
		};
    }
    
    public static Document getMetadata (String url) throws SAXException, IOException, ParserConfigurationException {
        DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
        fac.setNamespaceAware(true);
    	return fac.newDocumentBuilder().parse(url); 
    }
    
    public static String[] getArrayByXpathStr (Document doc, String xpathStr) throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();
		xpath.setNamespaceContext(XmlUtil.getMetadataNSCtx());
		XPathExpression expr = xpath.compile(xpathStr);
		NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		ArrayList<String> list = new ArrayList<String>();
		for (Node node : XmlUtil.asList(nodes)) {
			list.add(node.getNodeValue());
		}
    	return list.stream().toArray(String[]::new);
    }
    
    public static String[] getSPs(Document doc) {
    	try {
			return getArrayByXpathStr(doc, XPATH_FOR_SPENTITIES);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return new String[0];
		}
    }
    
    public static String[] getAcsHostsForEntity (Document doc, String entity) {
		String xpath = XPATH_FOR_SP_ACS.replaceFirst(
				GeneralStrings.getString("XmlUtil.EntityIDReplacementStr"), entity); //$NON-NLS-1$
		ArrayList<String> hostList = new ArrayList<String>();
		try {
			String[] acs = XmlUtil.getArrayByXpathStr(doc, xpath);
			for (String a: acs) {
				URL url = new URL(a);
				hostList.add(url.getHost());
			}
			return hostList.stream().distinct().toArray(String[]::new);
		} catch (XPathExpressionException | MalformedURLException e) {
			return new String[0];
		}
    }
    
    public static void printList(Document doc, DatabaseReader dbr) {
		for (String e: XmlUtil.getSPs(doc)) {
			System.out.println(e);
			try {
				for (String a: XmlUtil.getAcsHostsForEntity(doc, e)) {
					InetAddress ip;
					try {
						ip = InetAddress.getByName(a);
						Country c = dbr.country(ip).getCountry();
						System.out.println("   " + c.getIsoCode() + //$NON-NLS-1$
								"   " + a); //$NON-NLS-1$
					} catch (UnknownHostException e1) {
						System.out.println("   unknownHost: " + a); //$NON-NLS-1$
					}
				}
			} catch (IOException | GeoIp2Exception e1) {
				e1.printStackTrace();
			}
		}
    	
    }
    
    
    
    
}