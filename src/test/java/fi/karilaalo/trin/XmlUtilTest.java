package fi.karilaalo.trin;

import static org.junit.Assert.*;

import javax.xml.namespace.NamespaceContext;

import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.w3c.dom.Document;

@RunWith(JUnit4.class)
public class XmlUtilTest {
	
	Document doc;
	
	@Before
	public void init() throws Exception {
		doc = XmlUtil.getMetadata(
				GeneralStrings.getString("XmlUtil.HakaMetadataURL"));
	}
	

	@Test
	public void metadataNSCtxTest() {
		assertThat(XmlUtil.getMetadataNSCtx(), 
				IsInstanceOf.instanceOf(NamespaceContext.class));
	}
	
	@Test
	public void getMetadataTest() throws Exception {
		assertNotNull(doc);
		assertThat(doc, IsInstanceOf.instanceOf(Document.class));
	}
	
	@Test
	public void getSPsTest() throws Exception {
		assertTrue(XmlUtil.getSPs(doc).length > 0);
	}
	
	@Test
	public void getAcsHostsForEntityTest() {
		assertTrue(XmlUtil.getAcsHostsForEntity(doc,
				XmlUtil.getSPs(doc)[0]).length
				> 0);
	}

}
