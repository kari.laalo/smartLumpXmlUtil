package fi.karilaalo.trin;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.maxmind.db.CHMCache;
import com.maxmind.geoip2.DatabaseReader;

/**
 * Hello world!
 *
 */
public class App extends XmlUtil {
	
    public static void main( String[] args ) {
    	
    	DatabaseReader dbr = null;
    	try {
			dbr = new DatabaseReader
				.Builder(new File(GeneralStrings.getString("App.GeoIpDataBaseLocation"))) //$NON-NLS-1$
				.withCache(new CHMCache())
				.build();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
    	
    	for (String str: GeneralStrings.getFeds()) {
    		try {
				Document doc = XmlUtil.getMetadata(
						GeneralStrings.getString("XmlUtil." +
								str + "MetadataURL"));
				System.out.println("\n\n*** " + str + " ***\n");
				XmlUtil.printList(doc, dbr);
			} catch (SAXException | IOException | ParserConfigurationException e) {
				e.printStackTrace();
			}
    	}
    	
    	    	
    }
}
